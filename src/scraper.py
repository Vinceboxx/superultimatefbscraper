import calendar
import csv
import datetime
import os
import platform
import shutil
import signal
import subprocess
import sys
import urllib.request
import logging
import json
import time
from zipfile import ZipFile
import hashlib
import pyautogui

from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select

# -------------------------------------------------------------
# -------------------------------------------------------------


# Global Variables
POSTS = True
LIKE = False
COMMENTS = "Comments and subs"
ABOUT = False
FRIENDS = False
PHOTOS = False
VIDEOS = False

# If changed to True the date will be saved in unix time int
unix_time = False
driver = None

# whether to download photos or not
download_uploaded_photos = True
download_friends_photos = True

# whether to download the full image or its thumbnail (small size)
# if small size is True then it will be very quick else if its false then it will open each photo to download it
# and it will take much more time
friends_small_size = True
photos_small_size = True

total_scrolls = 2500
current_scrolls = 0
scroll_time = 8

old_height = 0
firefox_profile_path = "/home/zeryx/.mozilla/firefox/0n8gmjoz.bot"
facebook_https_prefix = "https://"

# Dictionary variable

d = {}

def init_log():
    logging.basicConfig(filename="last_execution.log",
                        filemode='w',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.INFO)

    logging.info("Program started")
init_log()

# -------------------------------------------------------------
# -------------------------------------------------------------


def read_dictionary():
    logging.info("Reading dictionary from dict.txt")
    try:
        with open("dict.txt") as f:
            content = f.readlines()
        dict = {}
        for line in content:
            l = line.split(' ', 1)
            dict[l[0]] = l[1][:-1]
        logging.info("Successfully read dictionary")
        return dict
    except:
        logging.info("Error reading dict.txt file")
        quit


d = read_dictionary()


def get_facebook_images_url(img_links):
    urls = []

    for link in img_links:
        if link != "None":
            valid_url_found = False
            driver.get(link)

            try:
                while not valid_url_found:
                    WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.CLASS_NAME, "spotlight")))
                    element = driver.find_element_by_class_name("spotlight")
                    img_url = element.get_attribute('src')

                    if img_url.find('.gif') == -1:
                        valid_url_found = True
                        urls.append(img_url)
            except Exception:
                urls.append("None")
        else:
            urls.append("None")

    return urls


def create_table_csv(data):
    csv_array = []
    csv_array.append(['timestamp', 'type', 'title', 'status', 'link', 'post_id', 'likes_num', 'comments_num', 'subcomments_num'])
    for element in data['posts']:
        if "like" in element:
            likes = element['like']['total']
        else:
            likes = 0
        if "comments" in element:
            if "comments_num" in element['comments']:
                comments = element['comments']['comments_num']
            else:
                comments = 0
            if "subcomments_num" in element['comments']:
                subcomments = element['comments']['subcomments_num']
            else:
                subcomments = 0
        else:
            comments = 0
            subcomments = 0
        csv_array.append([element['timestamp'], element['type'], element['title'], element['status'], element['link'], element['post_id'],
                   likes, comments, subcomments])
    return csv_array




# -------------------------------------------------------------
# -------------------------------------------------------------

# takes a url and downloads image from that url
def image_downloader(img_links, folder_name):
    img_names = []

    try:
        parent = os.getcwd()
        try:
            folder = os.path.join(os.getcwd(), folder_name)
            create_folder(folder)
            os.chdir(folder)
        except Exception:
            print("Error in changing directory.")

        for link in img_links:
            img_name = "None"

            if link != "None":
                img_name = (link.split('.jpg')[0]).split('/')[-1] + '.jpg'

                # this is the image id when there's no profile pic
                if img_name == "10354686_10150004552801856_220367501106153455_n.jpg":
                    img_name = "None"
                else:
                    try:
                        urllib.request.urlretrieve(link, img_name)
                    except Exception:
                        img_name = "None"

            img_names.append(img_name)

        os.chdir(parent)
    except Exception:
        print("Exception (image_downloader):", sys.exc_info()[0])

    return img_names


# -------------------------------------------------------------
# -------------------------------------------------------------

def check_height():
    new_height = driver.execute_script("return document.body.scrollHeight")
    return new_height != old_height


# -------------------------------------------------------------
# -------------------------------------------------------------

# helper function: used to scroll the page

def scroll():
    global old_height

    old_height = driver.execute_script("return document.body.scrollHeight")
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    WebDriverWait(driver, scroll_time, 0.05).until(lambda driver: check_height())
    global current_scrolls
    current_scrolls += 1

    return


# -------------------------------------------------------------
# -------------------------------------------------------------

# --Helper Functions for Posts

def get_status(x):
    status = ""
    life_event = False
    try:
        status = x.find_element_by_xpath(d['status1']).text  # use _1xnd for Pages
    except Exception:
        try:
            status = x.find_element_by_xpath(d['status-born']).text
            life_event = True
        except Exception:
            try:
                status = x.find_element_by_xpath(d['status-little-text']).text  # post status little text
            except Exception:
                try:
                    status = x.find_element_by_xpath(d['status-medium-text']).text  # post status medium text
                except Exception:
                    try:
                        status = x.find_element_by_xpath(d['status2']).text  # post born
                    except Exception:
                        try:
                            status = x.find_element_by_xpath(
                                d['status-large-text-with-bg']).text  # post status big text with foreground
                        except Exception:
                            logging.info("No status found on " + x.text)
                            pass

    return status, life_event


# Helper function to get the comment maker (works with comments and subcomments)
def get_comment_maker(x):
    cm = ""
    try:
        cm = x.find_element_by_xpath(d['comment-maker']).text
    except Exception:
        pass
    finally:
        return cm


# Helper function to get the comment text (works with comments and subcomments)
def get_comment_text(x):
    text = ""
    try:
        text = x.find_element_by_xpath(d['comment-text']).text
    except Exception:
        pass
    finally:
        return text


# Helper function to get the the whole content of a comment, including position used to check subcomments
# intentation level (works with comments and subcomments)
def get_single_comment(comment_block):
    try:
        comment = comment_block.find_element_by_xpath(d['comment-maker-and-text'])  # use _1xnd for Pages
        text = get_comment_text(comment)
        maker = get_comment_maker(comment)
        date = get_time(comment_block)
        c = {
            "timestamp": date,
            "maker": maker,
            "comment": text,
            "x": comment_block.location['x']
        }
    except Exception as e:
        logging.info("Error scraping comment " + comment_block.text)
    return c


# Helper function used to correctly construct the subcomments tree for a single first level comment
def order_subcomments(subcomments, positions):
    ordered = {'subcomment': []}
    try:
        for single in subcomments:
            if single['x'] == positions[0]:
                ordered['subcomment'].append(single)
            elif single['x'] == positions[1]:
                if 'subcomment' not in ordered["subcomment"][-1]:
                    ordered["subcomment"][-1]["subcomment"] = []
                ordered['subcomment'][-1]["subcomment"].append(single)
            elif single['x'] == positions[2]:
                if 'subcomment' not in ordered["subcomment"][-1]["subcomment"][-1]:
                    ordered["subcomment"][-1]["subcomment"][-1]["subcomment"] = []
                ordered['subcomment'][-1]["subcomment"][-1]["subcomment"].append(single)
    except Exception as e:
        print(e)
    return ordered


# Function used to get all the comments under a post, including all subcomments.
# It returns a list ordered of the comments as they appear under a post
def get_comments(x):
    data = {'comments': [], 'comments_num': 0, 'subcomments_num': 0}
    i = 0
    for replies in x.find_elements_by_xpath(d['comment-reply-click']):
        replies.click()
    try:

        for comment_block in x.find_element_by_xpath(d["comment-and-subs-blocks"]).find_elements_by_xpath(
                d["comment-block-main"]):
            # get first main comment (or 1st level comment) and appending to comment list
            data["comments"].append(get_single_comment(comment_block.find_elements_by_xpath(".//div")[0]))
            data['comments_num'] = data['comments_num'] + 1
            subcomments = []
            positions = []
            # searching for sub-comments up to 3rd level and eventually appending them to the main comment
            if COMMENTS == "Comments and Subs":
                for single in comment_block.find_element_by_xpath(d['subcomments-all-block']).find_elements_by_xpath(
                        d['subcomment-single-block']):
                    subcomments.append(get_single_comment(single))
                    positions.append(single.location['x'])
                if len(subcomments) > 0:
                    data['subcomments_num'] = data['subcomments_num'] + len(subcomments)
                    positions = list(dict.fromkeys(positions))
                    positions.sort()
                    data["comments"][i]["subcomments"] = order_subcomments(subcomments, positions)
            i = i + 1

    except Exception as e:
        logging.debug("No comments found on this post! " + str(e))
    return data


def get_div_links(x, tag):
    try:
        temp = x.find_element_by_xpath(d['div-links'])
        return temp.find_element_by_tag_name(tag)
    except Exception:
        try:
            temp = x.find_element_by_xpath(d['div-links2'])
            return temp.find_element_by_tag_name(tag)
        except:
            return ""


def get_title_links(title):
    l = title.find_elements_by_tag_name('a')
    return l[-1].text, l[-1].get_attribute('href').split('&')[0]


def get_title(x):
    title = ""
    try:
        title = x.find_element_by_xpath(d['title1'])
    except Exception:
        try:
            title = x.find_element_by_xpath(d['title2'])
        except Exception:
            try:
                title = x.find_element_by_xpath(d['title3'])
            except Exception:
                pass
    finally:
        return title


def get_like(x):
    driver.execute_script("window.scrollTo(" + str(x.location['x']) + "," + str(x.location['y']) + ", 0);")
    time.sleep(0.3)
    like = {}
    like["like"] = []
    like["total"] = 0
    try:
        reaction_element = x.find_element_by_xpath(d["reaction-element"])
        reaction_element.click()
        time.sleep(1.5)
        while True:
            try:
                driver.find_element_by_xpath(d["reaction-see-more"]).find_element_by_tag_name("a").click()
                time.sleep(2)
            except Exception as e:
                break

        reaction_block = driver.find_elements_by_xpath(d["reaction-block"])

        for single_like in reaction_block:
            person = {"name": single_like.text,
                      "link": single_like.find_element_by_xpath(".//*").get_attribute("href").split("?")[0]}
            like["like"].append(person)
            like["total"] = like["total"] + 1

        driver.find_element_by_xpath(d["reaction-close"]).click()
        time.sleep(1.5)

    except Exception as e:
        logging.debug("No likes found on this post!")
        pass
    finally:
        return like


def get_post_link(x):
    try:
        return x.find_element_by_xpath(d['div-links2']).find_element_by_tag_name('a').get_attribute('href')
    except:
        return ""


def get_time(x):
    post_time = ""
    try:

        post_time = x.find_element_by_tag_name(d['date']).get_attribute('data-utime')
        if not unix_time:
            post_time = time.ctime(int(post_time))
        if post_time != "":
            pass



    # except Exception:
    #     try:
    #         time = x.find_element_by_tag_name(d['date']).get_attribute('title')
    #         time = str("%02d" % int(time.split(", ")[1].split()[1]), ) + "-" + str(
    #             ("%02d" % (int((list(calendar.month_abbr).index(time.split(", ")[1].split()[0][:3]))),))) + "-" + \
    #                time.split()[3] + " " + str("%02d" % int(time.split()[5].split(":")[0])) + ":" + str(
    #             time.split()[5].split(":")[1])
    except Exception as e:
        logging.debug("No date found! Please check for any changes in attribute name!")
    finally:
        return post_time


def extract_and_write_posts(elements, filename):
    try:
        f = open(filename, "w", newline='\r\n')
        data = {"posts": []}
        current_dir = os.getcwd()
        i = 0

        for x in elements:
            try:
                title = " "
                status = " "
                link = ""
                time = " "
                life_event = False
                post_id = ""

                # time
                time = get_time(x)

                # title
                title = get_title(x)
                if title.text.find("shared a memory") != -1:
                    x = x.find_element_by_xpath(d['complete-post'])
                    title = get_title(x)

                status, life_event = get_status(x)
                if LIKE:
                    like = get_like(x)
                if COMMENTS != "No Comments":
                    comments = get_comments(x)

                type, link = get_type_and_link(x, title, status, life_event)
                link = link.split('&')[0]

                if not isinstance(title, str):
                    title = title.text

                status = status.replace("\n", " ")
                title = title.replace("\n", " ")
                #post_id = get_post_link(x)
                #post_id

                if "photo.php?fbid=" in link:
                    post_id = link.split('photo.php?fbid=')[1].split('&')[0]
                elif "profile.php?id=" in link:
                    post_id = link.split('profile.php?id=')[1].split('&')[0]
                elif "story_fbid=" in link:
                    post_id = link.split("story_fbid=")[1].split('&')[0]
                else:
                    post_id = link.split('/')[-1].split('?')[0]


                try:
                    create_folder("Screenshots")
                    os.chdir(os.path.join(os.getcwd(), "Screenshots"))
                    x.screenshot(post_id + ".png")
                except Exception as e:
                    pass
                finally:
                    os.chdir(current_dir)

                data["posts"].append({
                    "timestamp": time,
                    "type": type,
                    "title": title,
                    "status": status,
                    "link": link,
                    "post_id": post_id
                })
                # Adding comments and likes to the post only if they are present
                if COMMENTS != "No Comments":
                    if len(comments["comments"]) > 0:
                        data["posts"][i]["comments"] = []
                        #data["posts"][i]["comments"] = comments["comments"]
                        data["posts"][i]["comments"] = comments

                if LIKE:
                    if len(like["like"]) > 0:
                        data["posts"][i]["like"] = []
                        data["posts"][i]["like"] = like

                i = i + 1

            except Exception as e:
                logging.info("Error scraping post "+ str(link))
                pass

        try:
            f.writelines(json.dumps(data))
            csv_array= create_table_csv(data)
            with open("Posts.csv", "w", newline="") as f2:
                writer = csv.writer(f2)
                writer.writerows(csv_array)
        except Exception:
            logging.info("There was a problem in converting the data structure in JSON format")
            pass
        f.close()
    except Exception:
        logging.info("Error when saving posts")
        print("Exception (extract_and_write_posts)", "Status =", sys.exc_info()[0])

    return


# -------------------------------------------------------------
# -------------------------------------------------------------

def get_type_and_link(x, title, status, life_event):
    type = ""
    link = ""
    if title.text == driver.find_element_by_id("fb-timeline-cover-name").text:
        if life_event:
            type = "life event"
            #link = get_div_links(x, "a").get_attribute('href')
            link = get_post_link(x)
            # status = get_div_links(x, "a").text
        elif status == '':
            temp = get_div_links(x, "img")
            if temp == '':  # no image tag which means . it is not a life event
                link = get_div_links(x, "a").get_attribute('href')
                type = "status update without text"
            else:
                type = 'life event'
                link = get_div_links(x, "a").get_attribute('href')
                status = get_div_links(x, "a").text
        else:
            type = "status update"
            if get_div_links(x, "a") != '':
                link = get_post_link(x)

    elif title.text.find(" shared ") != -1:

        x1, link = get_title_links(title)
        type = "shared " + x1

    elif title.text.find(" at ") != -1 or title.text.find(" in ") != -1:
        if title.text.find(" at ") != -1:
            x1, link = get_title_links(title)
            type = "check in"
        elif title.text.find(" in ") != 1:
            status = get_div_links(x, "a").text

    elif title.text.find(" added ") != -1 and title.text.find("photo") != -1:
        type = "added photo"
        link = get_div_links(x, "a").get_attribute('href')

    elif title.text.find("profile") and title.text.find("picture") != -1:
        type = "profile picture change"
        link = get_div_links(x, "a").get_attribute('href').split('&')[0]

    elif title.text.find("cover") != -1 and title.text.find("photo") != -1:
        type = "cover photo change"
        link = get_div_links(x, "a").get_attribute('href').split('&')[0]

    elif title.text.find(" added ") != -1 and title.text.find("video") != -1:
        type = "added video"
        link = get_div_links(x, "a").get_attribute('href')

    elif title.text.find(" to\n") != -1:

        x1, link = get_title_links(title)
        type = "received post on timeline"

    elif title.text.find(" watching") != -1:

        x1, link = get_title_links(title)
        type = "watching an event/movie"

    elif title.text.find("is with") != -1:
        link = get_post_link(x)
        type = "tagged in a photo"

    else:
        type = "others"

    return type, link


def save_to_file(name, elements, status, current_section):
    """helper function used to save links to files"""

    # status 0 = dealing with friends list
    # status 1 = dealing with photos
    # status 2 = dealing with videos
    # status 3 = dealing with about section
    # status 4 = dealing with posts

    try:
        f = None  # file pointer

        if status != 4:
            f = open(name, 'w', encoding='utf-8', newline='\r\n')

        results = []
        img_names = []

        # dealing with Friends
        if status == 0:
            # get profile links of friends
            results = [x.get_attribute('href') for x in elements]
            results = [create_original_link(x) for x in results]

            # get names of friends
            people_names = [x.find_element_by_tag_name("img").get_attribute("aria-label") for x in elements]

            # download friends' photos
            try:
                logging.info("Downloading friends data")
                if download_friends_photos:
                    if friends_small_size:
                        img_links = [x.find_element_by_css_selector('img').get_attribute('src') for x in elements]
                    else:
                        links = []
                        for friend in results:
                            try:
                                driver.get(friend)
                                WebDriverWait(driver, 30).until(
                                    EC.presence_of_element_located((By.CLASS_NAME, "profilePicThumb")))
                                l = driver.find_element_by_class_name("profilePicThumb").get_attribute('href')
                            except Exception:
                                l = "None"

                            links.append(l)

                        for i, _ in enumerate(links):
                            if links[i] is None:
                                links[i] = "None"
                            elif links[i].find('picture/view') != -1:
                                links[i] = "None"

                        img_links = get_facebook_images_url(links)

                    folder_names = ["Friend's Photos", "Mutual Friends' Photos", "Following's Photos",
                                    "Follower's Photos", "Work Friends Photos",
                                    "College Friends Photos", "Current City Friends Photos", "Hometown Friends Photos"]
                    print("Downloading " + folder_names[current_section])

                    img_names = image_downloader(img_links, folder_names[current_section])
                else:
                    img_names = ["None"] * len(results)
            except Exception:
                logging.info("Error downloading friends profile images")
                print("Exception (Images)", str(status), "Status =", current_section, sys.exc_info()[0])

        # dealing with Photos
        elif status == 1:
            logging.info("Downloading photos")
            results = [x.get_attribute('href') for x in elements]
            results.pop(0)

            try:
                if download_uploaded_photos:
                    if photos_small_size:
                        background_img_links = driver.find_elements_by_xpath(d['background-img'])
                        background_img_links = [x.get_attribute('style') for x in background_img_links]
                        background_img_links = [((x.split('(')[1]).split(')')[0]).strip('"') for x in
                                                background_img_links]
                    else:
                        background_img_links = get_facebook_images_url(results)

                    folder_names = ["Uploaded Photos", "Tagged Photos"]
                    print("Downloading " + folder_names[current_section])

                    img_names = image_downloader(background_img_links, folder_names[current_section])
                else:
                    img_names = ["None"] * len(results)
            except Exception as e:
                logging.info("Error downloading photos")
                print("Exception (Images)", str(status), "Status =", current_section, sys.exc_info()[0])

        # dealing with Videos
        elif status == 2:
            logging.info("Downloading videos")
            results = elements[0].find_elements_by_css_selector('li')
            results = [x.find_element_by_css_selector('a').get_attribute('href') for x in results]

            try:
                if results[0][0] == '/':
                    results = [r.pop(0) for r in results]
                    results = [("https://en-gb.facebook.com/" + x) for x in results]
            except Exception:
                logging.info("Error downloading videos")
                pass

        # dealing with About Section
        elif status == 3:
            logging.info("Downloading about section")
            results = elements[0].text
            f.writelines(results)

        # dealing with Posts
        elif status == 4:
            logging.info("Downloading posts")
            extract_and_write_posts(elements, name)
            logging.info("Saving HTML page")
            pyautogui.hotkey('ctrl', 's')
            time.sleep(1)
            pyautogui.typewrite(os.getcwd() + os.path.sep + 'profile')
            pyautogui.hotkey('enter')
            with open("profile.html", "w") as f:
                f.write(driver.page_source)
            return

        """Write results to file"""
        if status == 0:
            logging.info("Write friends to file")
            for i, _ in enumerate(results):
                # friend's profile link
                f.writelines(results[i])
                f.write(',')

                # friend's name
                f.writelines(people_names[i])
                f.write(',')

                # friend's downloaded picture id
                f.writelines(img_names[i])
                f.write('\n')

        elif status == 1:
            logging.info("Write images to file")

            for i, _ in enumerate(results):
                # image's link
                f.writelines(results[i])
                f.write(',')

                # downloaded picture id
                f.writelines(img_names[i])
                f.write('\n')

        elif status == 2:
            for x in results:
                f.writelines(x + "\n")

        f.close()

    except Exception:
        logging.info("Error saving on file")

        print("Exception (save_to_file)", "Status =", str(status), sys.exc_info()[0])

    return


# ----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def scrape_data(user_id, scan_list, section, elements_path, save_status, file_names):
    """Given some parameters, this function can scrap friends/photos/videos/about/posts(statuses) of a profile"""
    page = []
    global logging

    if save_status == 4 :
        page.append(user_id)

    page += [user_id + s for s in section]

    for i, _ in enumerate(scan_list):
        try:
            driver.get(page[i])

            if (save_status == 0) or (save_status == 1) or (
                    save_status == 2):  # Only run this for friends, photos and videos

                # the bar which contains all the sections
                sections_bar = driver.find_element_by_xpath(d['section-bar'])

                if sections_bar.text.find(scan_list[i]) == -1:
                    continue

            if save_status != 3:
                if save_status == 4:  # sureply
                    for replies in driver.find_elements_by_xpath(d["comment-reply-click"]):
                        replies.click()
                global current_scrolls
                global total_scrolls
                try:
                    while current_scrolls < total_scrolls:
                        for replies in driver.find_elements_by_xpath(d["comment-reply-click"]):
                            replies.click()

                        scroll()

                except:
                    logging.info("Scrolling down web page done correctly")
                    logging.info("Existing comments correctly clicked")
                    pass

            data = driver.find_elements_by_xpath(elements_path[i])

            save_to_file(file_names[i], data, save_status, i)

        except Exception as e:
            print(e)
            print("Exception (scrape_data)", str(i), "Status =", str(save_status), sys.exc_info()[0])


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def create_original_link(url):
    if url.find(".php") != -1:
        original_link = facebook_https_prefix + ".facebook.com/" + ((url.split("="))[1])

        if original_link.find("&") != -1:
            original_link = original_link.split("&")[0]

    elif url.find("fnr_t") != -1:
        original_link = facebook_https_prefix + ".facebook.com/" + ((url.split("/"))[-1].split("?")[0])
    elif url.find("_tab") != -1:
        original_link = facebook_https_prefix + ".facebook.com/" + (url.split("?")[0]).split("/")[-1]
    else:
        original_link = url

    return original_link


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def create_folder(folder):
    logging.info("Creating Folder " +str(folder) + " if doesn't exist")
    if not os.path.exists(folder):
        os.mkdir(folder)


def change_language(language):
    driver.get("https://www.facebook.com/settings?tab=language&section=account&view")
    select = Select(driver.find_element_by_xpath("//select[@name='new_language']"));
    current = select.first_selected_option.get_attribute("value")
    select.select_by_value(language)
    driver.find_element_by_id("u_0_9").click()
    return current


def scrap_profile(ids):
    folder = os.path.join(os.getcwd(), "Data")
    create_folder(folder)
    os.chdir(folder)
    # execute for all profiles given in input.txt file
    for user_id in ids:

        driver.get(user_id)
        url = driver.current_url
        user_id = create_original_link(url)
        logging.info("Scraping Profile: ", user_id)
        print("\nScraping:", user_id)
        try:
            target_dir = os.path.join(folder, user_id.split('/')[-1])
            create_folder(target_dir)
            os.chdir(target_dir)
        except Exception:
            print("Some error occurred in creating the profile directory.")
            continue
            # ----------------------------------------------------------------------------
        print("----------------------------------------")

        if FRIENDS:
            logging.info("Start Friends Scraping")
            print("Friends..")
            # setting parameters for scrape_data() to scrape friends
            scan_list = ["All", "Mutual Friends", "Following", "Followers", "Work", "College", "Current City",
                         "Hometown"]
            section = ["/friends", "/friends_mutual", "/following", "/followers", "/friends_work", "/friends_college",
                       "/friends_current_city",
                       "/friends_hometown"]
            elements_path = ["//*[contains(@id,'pagelet_timeline_medley_friends')][1]/div[2]/div/ul/li/div/a",
                             "//*[contains(@id,'pagelet_timeline_medley_friends')][1]/div[2]/div/ul/li/div/a",
                             "//*[contains(@class,'_3i9')][1]/div/div/ul/li[1]/div[2]/div/div/div/div/div[2]/ul/li/div/a",
                             "//*[contains(@class,'fbProfileBrowserListItem')]/div/a",
                             "//*[contains(@id,'pagelet_timeline_medley_friends')][1]/div[2]/div/ul/li/div/a",
                             "//*[contains(@id,'pagelet_timeline_medley_friends')][1]/div[2]/div/ul/li/div/a",
                             "//*[contains(@id,'pagelet_timeline_medley_friends')][1]/div[2]/div/ul/li/div/a",
                             "//*[contains(@id,'pagelet_timeline_medley_friends')][1]/div[2]/div/ul/li/div/a"]
            file_names = ["All Friends.txt", "Mutual Friends.txt", "Following.txt", "Followers.txt", "Work Friends.txt",
                          "College Friends.txt",
                          "Current City Friends.txt", "Hometown Friends.txt"]
            save_status = 0

            scrape_data(user_id, scan_list, section, elements_path, save_status, file_names)
            logging.info("Friends scraping ended")
            print("Friends Done!")

        # ----------------------------------------------------------------------------

        print("----------------------------------------")
        if PHOTOS:
            print("Photos..")
            print("Scraping Links..")
            # setting parameters for scrape_data() to scrap photos
            scan_list = ["'s Photos", "Photos of"]
            section = ["/photos_all", "/photos_of"]
            elements_path = ["//*[contains(@id, 'pic_')]"] * 2
            file_names = ["Uploaded Photos.txt", "Tagged Photos.txt"]
            save_status = 1

            scrape_data(user_id, scan_list, section, elements_path, save_status, file_names)
            print("Photos Done!")

        # ----------------------------------------------------------------------------
        print("----------------------------------------")
        if VIDEOS:
            print("Videos:")
            # setting parameters for scrape_data() to scrap videos
            scan_list = ["'s Videos", "Videos of"]
            section = ["/videos_by", "/videos_of"]
            elements_path = ["//*[contains(@id, 'pagelet_timeline_app_collection_')]/ul"] * 2
            file_names = ["Uploaded Videos.txt", "Tagged Videos.txt"]
            save_status = 2

            scrape_data(user_id, scan_list, section, elements_path, save_status, file_names)
            print("Videos Done!")
        # ----------------------------------------------------------------------------
        if ABOUT:
            print("----------------------------------------")
            print("About:")
            # setting parameters for scrape_data() to scrap the about section
            scan_list = [None] * 7
            section = ["/about?section=overview", "/about?section=education", "/about?section=living",
                       "/about?section=contact-info", "/about?section=relationship", "/about?section=bio",
                       "/about?section=year-overviews"]
            elements_path = ["//*[contains(@id, 'pagelet_timeline_app_collection_')]/ul/li/div/div[2]/div/div"] * 7
            file_names = ["Overview.txt", "Work and Education.txt", "Places Lived.txt", "Contact and Basic Info.txt",
                          "Family and Relationships.txt", "Details About.txt", "Life Events.txt"]
            save_status = 3

            scrape_data(user_id, scan_list, section, elements_path, save_status, file_names)
            print("About Section Done!")

        # ----------------------------------------------------------------------------
        if POSTS:
            print("----------------------------------------")
            print("Posts:")
            # setting parameters for scrape_data() to scrap posts
            scan_list = [None]
            section = []
            elements_path = [d['post-id']]  # Complete post ID

            file_names = ["Posts.json"]
            save_status = 4

            scrape_data(user_id, scan_list, section, elements_path, save_status, file_names)
            print("Posts(Statuses) Done!")
            print("----------------------------------------")
    # ----------------------------------------------------------------------------

    print("\nProcess Completed.")

    return


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def safe_find_element_by_id(driver, elem_id):
    try:
        return driver.find_element_by_id(elem_id)
    except NoSuchElementException:
        return None

def safe_find_element_by_xpath(elem_id):
    try:
        return driver.find_element_by_xpath(elem_id)
    except NoSuchElementException:
        logging.info("Element '"+ elem_id+ "' not found")
        return None


def login(email, password):
    """ Logging into our own profile """

    try:
        global driver

        options = Options()

        #  Code to disable notifications pop up of Chrome Browser
        options.add_argument("--disable-notifications")
        options.add_argument("--disable-infobars")
        options.add_argument("--mute-audio")
        options.add_argument("--no-sandbox")
        # options.add_argument("headless")

        try:
            platform_ = platform.system().lower()
            if platform_ in ['linux', 'darwin']:
                os.system("xhost +")
                driver = webdriver.Chrome(executable_path="./chromedriver", options=options)
            else:
                driver = webdriver.Chrome(executable_path="./chromedriver.exe", options=options)
        except Exception as e:
            print (e)
            logging.info("No chromedriver found")
            print("Kindly replace the Chrome Web Driver with the latest one from "
                  "http://chromedriver.chromium.org/downloads "
                  "and also make sure you have the latest Chrome Browser version."
                  "\nYour OS: {}".format(platform_)
                  )
            exit()

        fb_path = facebook_https_prefix + "facebook.com"
        driver.get(fb_path)
        driver.maximize_window()

        # filling the form
        driver.find_element_by_name('email').send_keys(email)
        driver.find_element_by_name('pass').send_keys(password)

        # clicking on login button
        try:
            driver.find_element_by_id('loginbutton').click()
        except:
            driver.find_element_by_name("login").click()

        # if your account uses multi factor authentication
        mfa_code_input = safe_find_element_by_id(driver, 'approvals_code')

        if mfa_code_input is None:
            return
        logging.info("Second Factor required")
        mfa_code_input.send_keys(input("Enter MFA code: "))
        driver.find_element_by_id('checkpointSubmitButton').click()

        # there are so many screens asking you to verify things. Just skip them all
        while safe_find_element_by_id(driver, 'checkpointSubmitButton') is not None:
            dont_save_browser_radio = safe_find_element_by_id(driver, 'u_0_3')
            if dont_save_browser_radio is not None:
                dont_save_browser_radio.click()

            driver.find_element_by_id('checkpointSubmitButton').click()

    except Exception:
        logging.info("Login failed")
        print("There's some error in log in.")
        print(sys.exc_info()[0])
        exit()
    logging.info("Login successful")

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------


def hash_file(fname ,type):
    if type=="MD5":
        hash = hashlib.md5()
    elif type=="SHA":
        hash = hashlib.sha512()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash.update(chunk)
    return hash.hexdigest()


def get_all_file_paths(directory):
    # initializing empty file paths list
    file_paths = []
    os.chdir("../..")
    # crawling through directory and subdirectories
    for root, directories, files in os.walk(directory):
        for filename in files:
            # join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)

            # returning all file paths
    return file_paths

def compress_and_hash():
    # path to folder which needs to be zipped
    directory = './Data'

    # calling function to get all file paths in the directory
    file_paths = get_all_file_paths(directory)
    try:
        # printing the list of all files to be zipped
        print('Following files will be zipped:')
        for file_name in file_paths:
            print(file_name)
            logging.info("Adding "+file_name+" to compressed archive")

            # writing files to a zipfile
        timestamp = str(datetime.datetime.now())
        with ZipFile('DATA ' + timestamp + ".zip", 'w') as zip:
            # writing each file one by one
            for file in file_paths:
                zip.write(file)
        print('All files zipped successfully!')
        logging.info('All files zipped successfully!')
    except:
        logging.info("Failed to create archive")
        return
    try:
        with open("hash.txt", 'w') as file:
            md5 = str(hash_file('DATA ' + timestamp + ".zip", "MD5"))
            file.write("MD5: " +md5+ "\n")
            sha = str(hash_file('DATA ' + timestamp + ".zip", "SHA"))
            file.write("SHA512: " + sha)
        logging.info("MD5: "+ md5)
        logging.info("SHA512: "+ sha)
    except:
        logging.info("Failed to calculate checksums")

def main():

    # Setting up debug options
    if len(sys.argv) > 1 and (sys.argv[1] == "--debug" or sys.argv[1] == "-d"):
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
    logging.info("Reading credentials from credentials.txt file")
    with open('credentials.txt') as f:
        email = f.readline().split('"')[1]
        password = f.readline().split('"')[1]

        if not (email and password):
            logging.info("E-mail and password not found in the file")
            print("Your email or password is missing. Kindly write them in credentials.txt")
            exit()

    ids = [facebook_https_prefix + "facebook.com/" + line.split("/")[-1] for line in open("input.txt", newline='\n')]


    try:
        process = subprocess.Popen(["python3", "network_capture.py"])
        logging.info("Starting Network capture")

        if len(ids) > 0:
            logging.info("Starting Scraping...")
            print("\nStarting Scraping...")

            login(email, password)
            logging.info("Changing language to English")
            current_language = change_language("en_US")
            scrap_profile(ids)
            logging.info("Restoring language")
            change_language(current_language)
            driver.close()
            logging.info("Ended scraping")
            logging.info("Creating ZIP archive and calculating hash values on it")

            logging.info("Stopping Network capture")
            process.send_signal(signal.SIGINT)
            time.sleep(2)

            compress_and_hash()
        else:
            logging.info("Input file is empty")
            print("Input file is empty.")
    except:
        pass
    finally:

        #shutil.rmtree("./Data")

        logging.info("Program execution terminated")
        quit()


# -------------------------------------------------------------
# -------------------------------------------------------------
# -------------------------------------------------------------

def run_scraper_from_gui(parameters):
    try:

        process = subprocess.Popen(["python3", "network_capture.py"])
        logging.info("Starting Network capture")


        email = parameters["email"]
        password = parameters["password"]
        global POSTS, LIKE, COMMENTS, ABOUT, FRIENDS, PHOTOS, VIDEOS, unix_time
        POSTS = parameters["posts"]
        LIKE = parameters["like"]
        COMMENTS = parameters["comments"]
        ABOUT = parameters["about"]
        FRIENDS = parameters["friends"]
        PHOTOS = parameters["photos"]
        VIDEOS = parameters["videos"]
        unix_time = parameters["unix_time"]
        printer = dict(parameters)
        printer["password"] = "***"
        ids = [parameters["target"]]
        logging.info("Input parameters:" + str(printer))

        if len(ids) > 0:
            print("\nStarting Scraping...")
            logging.info("Starting Scraping...")
            login(email, password)
            logging.info("Changing language to English")
            #current_language = change_language("en_US")
            scrap_profile(ids)
            logging.info("Restoring language")
            #change_language(current_language)
            driver.close()
            logging.info("Ended scraping")
            logging.info("Creating ZIP archive and calculating hash values on it")

            logging.info("Stopping Network capture")
            process.send_signal(signal.SIGINT)
            time.sleep(2)

            compress_and_hash()



        else:
            print("Input file is empty.")
    except:
        pass
    finally:

        logging.info("Program execution terminated")
        #shutil.rmtree("./Data")

        quit()


if __name__ == '__main__':
    # get things rolling
    main()
