#!/usr/bin/python3
import subprocess
from threading import Thread
from tkinter import *
from tkinter import messagebox

import scraper


root = Tk()
root.geometry("500x300")
root.title("SuperUltimateFacebookScraper")
post = IntVar()
c_post = Checkbutton(root, text="Post",variable=post)
c_post.grid(column=1, row=1)


def run_scrap():
    parameters={}
    parameters["posts"]=bool(post.get())
    parameters["like"]=bool(like.get())
    parameters["email"]=str(text_email.get())
    parameters["target"]=str(text_target.get())
    parameters["password"]=str(text_password.get())
    parameters["about"]=bool(about.get())
    parameters["friends"]=bool(friend.get())
    parameters["photos"]=bool(photos.get())
    parameters["unix_time"]=bool(unixTime.get())
    parameters["videos"]=bool(videos.get())
    parameters["comments"]="No Comments"
    for i in range(1,len(values)):
        if values[list(values.keys())[i]] ==comments.get():
            parameters["comments"] = str(list(values.keys())[comments.get()] )
    output=parameters
    print(output)
    scraper.run_scraper_from_gui(parameters)
    messagebox.showinfo("Success!", "Process completed!")
    quit()

like = IntVar()
c_like = Checkbutton(root, text="Like",variable=like)
c_like.grid(column=1, row=2)


values = {"No Comments" : 0,
         "Comments only" : 1,
          "Comments and Subs" : 2}
i=3
comments=IntVar()
for (text, value) in values.items():
    Radiobutton(root, text=text, variable=comments,
                value=value,justify=LEFT).grid(column=1, row=i)
    i=i+1

friend = IntVar()
c_friend = Checkbutton(root, text="Friend",variable=friend)
c_friend.grid(column=1, row=6)

photos = IntVar()
c_photos = Checkbutton(root, text="Photos",variable=photos)
c_photos.grid(column=1, row=7)

videos = IntVar()
c_videos = Checkbutton(root, text="Videos",variable=videos)
c_videos.grid(column=1, row=8)

about = IntVar()
c_about = Checkbutton(root, text="About",variable=about)
c_about.grid(column=1, row=9)

unixTime = IntVar()
c_unixTime = Checkbutton(root, text="Use UNIX Time",variable=unixTime)
c_unixTime.grid(column=1, row=10)


c_email=Label(root, text="Email")
c_email.grid(column=3, row=2)

text_email=Entry(root, text="email")
text_email.grid(column=4, row=2)

c_password=Label(root, text="Password")
c_password.grid(column=3, row=3)

text_password=Entry(root, text="Password",show='*')
text_password.grid(column=4, row=3)

c_target=Label(root, text="Profile to Scrap")
c_target.grid(column=3, row=6)

text_target=Entry(root, text="")
text_target.grid(column=4, row=6)

button_quit=Button(root, text="Quit")
button_quit.grid(column=3, row=8)
button_quit["command"] = quit
button_quit["fg"] = "red"

button_run=Button(root, text="Run")
button_run.grid(column=4, row=8)
button_run["command"] =run_scrap

button_run["fg"] = "green"

space=Label(root, text="      ")
space.grid(column=2, row=3)


root.mainloop()